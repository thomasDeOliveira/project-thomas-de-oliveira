# project thomas de oliveira

## Erxercice 0

Premiere étape creer un conteneur avec l'image gitlab-runner avec la commande :
docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

Faire la commande docker ps pour recuperer l'id du conteneur

Rentrer dans le docker gitlab-runner avec la commande:
docker exec -it id_conteneur bash 

Sur le gitlab dans les settings CI-CD creer un runner et copier la commande dans le bash
gitlab-runner register  --url https://gitlab.com  --token le token donner par gitlab

choisissez un executeur docker avec une image latest 

Faites un apt update et apt install nano

rentrer dans la config.toml en faisant la commande : 
nano /etc/gitlab-runner/config.toml
et modifier le volume en lui mettant : ["/var/run/docker.sock:/var/run/docker.sock"]

Ensuite creer un nouveau conteneur a partir du Dockerfile: 
ici notre dockerfile utilise la derniere image node
il crée un dossier app
dedans il y copie notre package.json et le package-lock.json
ensuite il fais un npm install
il copie tout les autres fichier
et enfin il lance un npm run build

Pour tester le dockerfile vous pouvez faire cette commande : 
docker build -t "le nom que vous voulez donnez" .

## Exercice 1

Pour activer AutoDevops il faut aller sur gitlab dans settings -> CI CD
Dans la parti Autodevops cocher pour l'activer et sélectionner la troisieme case a cocher :
Automatic deployment to staging, manual deployment to production 
![Alt text](image.png)

Nous choisissons celui la pour plus de sécurité car nous voulons que ce sois automatique pour tout les stage mais manuel pour le déploiement en production

Une fois cette étape faites il nous faut créer un nouveau runner qui servira pour la partie codeQuality des jobs de la pipeline créer par AutoDevops. Sans oublier de décocher Enable shared runners for this project pour que le projet utilise bien nos runners
Retournons dans notre conteneur gitlab-runner en reprennant les étapes précédantes, pareil pour la création d'un runner.
Nous choisissons un executeur docker avec une image latest encore.
Et pour celui nous remplacons le volume par : ["/cache", "/var/run/docker.sock:/var/run/docker.sock", "/tmp/builds:/tmp/builds"]
![Alt text](image-1.png)

Ensuite nous retournons sur gitlab et nous modifions notre premier runner en cochant la case :
Indicates whether this runner can pick jobs without tags
pour que ce runner sois utiliser pour les stages qui ne possède pas de tags
![Alt text](image-2.png)

Dans notre .gitlab-ci.yml nous précisons donc que nous ne voulons pas faire le stage test qui est déprécier et donc qui ne fonctionnerai pas pour le moment 
Et nous précisons également que le stage codeQuality doit utiliser notre deuxieme runner que nous venons de créer.

## Exercice 2:

Pour faire fonctionner gitlab Pages j'ai du modifier mon fichier next.config.js pour lui ajouter un assetPrefix et outpout = export assetPrefix va me permettre de pouvoir recuperer mon css et le outpout = export c'est pour creer un fichier out contenant mon index.html lors du npm run build.

Ensuite j'ai creer deux environnement distinct dans gitlab environnement
![Alt text](image-3.png)

Et sur .gitlab-ci.yml j'ai différencier les deux déploiements en mettant des regles différentes et les environnements qu'il convenait.
Pour le deploiement vers le conteneur docker je l'ai mis sur l'environnement de staging et je lui ai mis une regle pour qu'il s'execute uniquement lorsque le push n'est pas sur la branche main. Tandis que pour Pages j'ai mis l'environnement de production et que le deploiement s'execute uniquement lorsque le push est sur la branche main

Pour le déploiment sur gitlab Pages j'utilise une image node et ensuite je fais un npm i pour installer tout les packages, je fais un npm run build qui va créer un fichier out ce fichier je copie dans un dossier public et c'est ce fichier dans lequel il y a la page de mon site static qui est renvoyer par gitlab Pages.

## Exercice 3

Dans un premier temps : J'ai esssayer de mettre en place Owasp Zap avec une image docker. Je run le docker avec un certains volume et je lui dis de vérifier ma gitlab Pages et de mettre faire un rapport dans /zap/wrk/report.html cependant pour l'instant j'ai un probleme lié a l'accès du dossier /zap/wrk/.

J'ai fais ce run de mon côté pour vérifier si le dossier /zap/wrk existait bien et oui il existe.

J'ai essayer de mettre toute les autorisations avec le chmod mais le problème a persister alors que lorsque je test directement sur ubuntu cela fonctionne.

voici le code que j'avais essayer : 
la commande complete du docker run est : 
- docker run -v $(pwd):/zap/wrk/ --name zap owasp/zap2docker-weekly zap-api-scan.py -t https://thomasDeOliveira.gitlab.io/project-thomas-de-oliveira -f openapi -r /zap/wrk/report.html || true
![Alt text](image-4.png)

Dans un second temps: j'ai trouver un tutoriel Youtube et c'est le scan que j'ai mis en place dans lequel cette fois ci j'utilise l'image owasp/zap2docker-stable cette fois ci je cree moi meme le dossier /zap/wrk/ et dans lequel je copie le resultat du scanner zap dans un fichier report.html et enfin apres le script je lui dis que s'il trouve quelque chose dans le report.html il stop la pipeline.