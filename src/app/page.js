import Image from 'next/image'
import styles from './page.module.css'
import crewMembersData from './crewMembers.json'

export default function Home() {
  return (
    <main className={styles.main}>
      <div className={styles.center}>
        <Image
          className={styles.logo}
          src="/one-piece-logo.png"
          alt="One Piece Logo"
          width={250}
          height={250}
          priority
        />
      </div>

      <div className={styles.grid}>
        {crewMembersData.map((member, index) => (
          <div key={index} className={styles.card}>
            <h2>
              {member.name} <span>- {member.role}</span>
            </h2>
            <Image
              src={member.image}
              alt={`${member.name} Image`}
              className={styles.memberImage}
              width={200}
              height={200}
              priority
            />
          </div>
        ))}
      </div>
    </main>
  );
}
